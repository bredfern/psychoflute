psychoflute
===========

An example of using csound as a high end library for an android audio application. You will need a copy of the free and open source csound library to  build this app, get it from [sourceforge here](http://sourceforge.net/projects/csound/files/csound5/Android/).
